﻿
using RPG_Heroes.EquipmentStrat.Armor.Interfaces;
using RPG_Heroes.EquipmentStrat.Armory;
using RPG_Heroes.HeroCreator;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Heroes.Armory
{
    public class Armor
    {
        public string Name { get; set; }
        public int EquipmentLvl { get; set; }
        public Stats ArmorBaseStats { get; set; }
        public ArmorType ArmorType { get; set; }
        public ArmorSlots ArmorSlots { get; set; }
        public IArmorStrategy ArmorRef { get; set; }


        public Armor (string name, int equipmentLvl, ArmorType armorType, IArmorStrategy armorRef, ArmorSlots armorSlots)
        {
            //General Armor Constructor, implements GenerateBaseStats from the various armors classes
            Name = name;
            EquipmentLvl = equipmentLvl;
            ArmorType = armorType;
            ArmorRef = armorRef;
            ArmorSlots = armorSlots;

            ArmorBaseStats = armorRef.GenerateBaseStats(new Stats(), EquipmentLvl, ArmorSlots);

        }
    }
}
