﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Heroes.EquipmentStrat.Armory
{
    public enum ArmorSlots
    {
        //Enums containig the avilable slots and weights for its respective slot
        Body=100,
        Head=80,
        Legs=60
    }
}
