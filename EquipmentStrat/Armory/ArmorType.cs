﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Heroes.Armory
{
    public enum ArmorType
    {
        //Enum containg options for the types of armor.
        PlateArmor,
        LeatherArmor,
        ClothArmor
    }
}
