﻿using RPG_Heroes.EquipmentStrat.Armor.Interfaces;
using RPG_Heroes.HeroCreator;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Heroes.EquipmentStrat.Armory.Armors
{
    class ClothArmorStrategy : IArmorStrategy
    {
        public ClothArmorStrategy()
        {

        }

        //Generates bases stats for Cloth Armor piece bases on slot and equipmentlvl
        public Stats GenerateBaseStats(Stats stats, int equipLvl, ArmorSlots armorSlots)
        {
            double scale = (int)armorSlots;
            scale /= 100;

            if (equipLvl == 1)
            {
                stats.Health = (int)(10 * scale);
                stats.Intelligence = (int)(3 * scale);
                stats.Dexterity = (int)(1 * scale);
            }
            else
            {
                stats.Health = (int)(10 + (5 * (equipLvl - 1)) * scale);
                stats.Intelligence = (int)(3 + (2 * (equipLvl - 1)) * scale);
                stats.Dexterity = (int)(1 + (1 * (equipLvl - 1)) * scale);
            }


            return stats;
        }

    }
}
