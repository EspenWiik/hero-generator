﻿using RPG_Heroes.EquipmentStrat.Armor.Interfaces;
using RPG_Heroes.HeroCreator;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Heroes.EquipmentStrat.Armory.Armors
{
    class LeatherArmorStrategy : IArmorStrategy
    {
        public LeatherArmorStrategy ()
        {

        }

        //Generates bases stats for Leather Armor piece bases on slot and equipmentlvl
        public Stats GenerateBaseStats(Stats stats, int equipLvl, ArmorSlots armorSlots)
        {
            double scale = (int)armorSlots;
            scale /= 100;

            if (equipLvl == 1)
            {
                stats.Health = (int)(20 * scale);
                stats.Strength = (int)(1 * scale);
                stats.Dexterity = (int)(3 * scale);
            }
            else
            {
                stats.Health = (int)(20 + (12 * (equipLvl - 1)) * scale);
                stats.Strength = (int)(1 + (1 * (equipLvl - 1)) * scale);
                stats.Dexterity = (int)(3 + (2 * (equipLvl - 1)) * scale);
            }


            return stats;
        }
    }
}
