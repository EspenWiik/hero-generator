﻿using RPG_Heroes.Armory;
using RPG_Heroes.EquipmentStrat.Armor.Interfaces;
using RPG_Heroes.HeroCreator;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Heroes.EquipmentStrat.Armory.Armors
{
    public class PlateArmorStrategy : IArmorStrategy
    {
        public PlateArmorStrategy()
        {

        }

        //Generates bases stats for Plate Armor piece bases on slot and equipmentlvl
        public Stats GenerateBaseStats(Stats stats, int equipLvl, ArmorSlots armorSlots)
        {
            double scale = (int)armorSlots;
            scale /= 100; 

            if(equipLvl == 1)
            {
                stats.Health = (int)(30 * scale);
                stats.Strength = (int)(3 * scale);
                stats.Dexterity = (int)(1 * scale);
            }
            else
            {
                stats.Health = (int)(30 + (12 * (equipLvl - 1)) * scale);
                stats.Strength = (int)(3 + (2 * (equipLvl - 1)) * scale);
                stats.Dexterity = (int)(1 + (1 * (equipLvl - 1)) * scale);
            }


            return stats;
        }


    }
}
