﻿using RPG_Heroes.Armory;
using RPG_Heroes.HeroCreator;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Heroes.EquipmentStrat.Armor.Interfaces
{
    public interface IArmorStrategy
    {
        //Interface for Armors
        Stats GenerateBaseStats(Stats stats, int equipLvl, Armory.ArmorSlots armorSlots);
        
    }
}
