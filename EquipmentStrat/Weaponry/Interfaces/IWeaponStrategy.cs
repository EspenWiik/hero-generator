﻿using RPG_Heroes.HeroCreator;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Heroes.EquipmentStrat.Weaponry.Interfaces
{
    public interface IWeaponStrategy
    {
        // Interface for the various weapons classes
        public int CalcBaseWeaponDmg(int weaponLvl);
        public int SetHeroDmg(Stats stats, int baseDmg);
    }
}
