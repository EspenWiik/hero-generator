﻿using RPG_Heroes.EquipmentStrat.Weaponry.Interfaces;
using RPG_Heroes.EquipmentStrat.Wepons;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Heroes.EquipmentStrat.Weapons
{
    public class Weapon
    {
        public string Name { get; set; }
        public int BaseDamage { get; set; }
        public int WeaponLvl { get; set; }
        public WeaponType WeaponType { get; set; }
        public IWeaponStrategy IWeaponStrategy { get; set; }

        //Constructor for Weapons, 
        public Weapon(string name, int weaponLvl, WeaponType weaponType, IWeaponStrategy iWeaponStrategy)
        {
            Name = name;
            WeaponLvl = weaponLvl;
            WeaponType = weaponType;
            IWeaponStrategy = iWeaponStrategy;

            BaseDamage = iWeaponStrategy.CalcBaseWeaponDmg(WeaponLvl);
        }

    }
}
