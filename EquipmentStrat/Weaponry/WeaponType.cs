﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Heroes.EquipmentStrat.Wepons
{
    //Enum for the various WeaponTypes
    public enum WeaponType
    {
        Melee,
        Ranged,
        Magic
    }
}
