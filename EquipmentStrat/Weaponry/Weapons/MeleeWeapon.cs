﻿using RPG_Heroes.EquipmentStrat.Weaponry.Interfaces;
using RPG_Heroes.HeroCreator;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Heroes.EquipmentStrat.Weponry.Wepons
{
    public class MeleeWeapon : IWeaponStrategy
    {

        public MeleeWeapon()
        {
            
        }

        //Method that returns BaseWeaponDmg for MeleeWeapons bases on baseStats + weaponLvl
        public int CalcBaseWeaponDmg(int weaponLvl)
        {
            int baseDmg = 15;
            if (weaponLvl > 1)
            {
                baseDmg += (2 * (weaponLvl - 1));
            }

            return baseDmg;
        }

        //Method applied when Hero wields weapon
        public int SetHeroDmg (Stats stats, int baseDmg)
        {
            int damage = baseDmg + (int)(stats.Strength * 1.5);
            return damage;
        }
    }
}
