﻿using RPG_Heroes.EquipmentStrat.Weaponry.Interfaces;
using RPG_Heroes.HeroCreator;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Heroes.EquipmentStrat.Weponry.Wepons
{
    public class RangedWeapon : IWeaponStrategy 
    {
        public RangedWeapon ()
        {

        }

        //Method that returns BaseWeaponDmg for RangedWeapons bases on baseStats + weaponLvl
        public int CalcBaseWeaponDmg(int weaponLvl)
        {
            int baseDmg = 5;
            if (weaponLvl > 1)
            {
                baseDmg += (3 * (weaponLvl - 1));
            }

            return baseDmg;
        }

        //Method applied when Hero wields weapon
        public int SetHeroDmg(Stats stats, int baseDmg)
        {
            int damage = baseDmg + (stats.Dexterity * 2);
            return damage;
        }
    }
}
