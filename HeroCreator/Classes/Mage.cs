﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Heroes.HeroCreator.Classes
{
    public class Mage : Hero
    {
        //Constructor with base stats for Mage
        public Mage() : base()
        {
            this.BaseStats.Health = 100;
            this.BaseStats.Strength = 2;
            this.BaseStats.Dexterity = 3;
            this.BaseStats.Intelligence = 10;
        }
        // Grants stats to hero when lvl up
        public override void LvlUp()
        {
            this.BaseStats.Health += 15;
            this.BaseStats.Strength += 1;
            this.BaseStats.Dexterity += 2;
            this.BaseStats.Intelligence += 5;

        }
    }
}
