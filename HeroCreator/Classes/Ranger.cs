﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Heroes.HeroCreator.Classes
{
    public class Ranger : Hero
    {
        //Constructor with base stats for Ranger
        public Ranger() :base()
        {
            this.BaseStats.Health = 120;
            this.BaseStats.Strength = 5;
            this.BaseStats.Dexterity = 10;
            this.BaseStats.Intelligence = 2;
        }
        // Grants stats to hero when lvl up
        public override void LvlUp()
        {
            this.BaseStats.Health += 20;
            this.BaseStats.Strength += 2;
            this.BaseStats.Dexterity += 5;
            this.BaseStats.Intelligence += 1;
        }
    }
}
