﻿using System;
using System.Collections.Generic;
using System.Text;


namespace RPG_Heroes.HeroCreator.Classes
{
    public class Warrior : Hero
    {
        //Constructor with base stats for Warrior
        public Warrior() :base()
        {
            this.BaseStats.Health = 150;
            this.BaseStats.Strength = 10;
            this.BaseStats.Dexterity = 3;
            this.BaseStats.Intelligence = 1;
        }
        // Grants stats to hero when lvl up
        public override void LvlUp()
        {
            this.BaseStats.Health += 30;
            this.BaseStats.Strength += 5;
            this.BaseStats.Dexterity += 2;
            this.BaseStats.Intelligence += 1;
        }
    }

 



}
