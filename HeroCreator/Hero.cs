﻿using RPG_Heroes.Armory;
using RPG_Heroes.EquipmentStrat.Armory;
using RPG_Heroes.EquipmentStrat.Weapons;
using System;
using System.Collections.Generic;


namespace RPG_Heroes.HeroCreator
{
    public abstract class Hero 
    {
        private int _level = 1;

        public int Level 
        {
            get => _level;
            set // lisstens for changes in lvl and runs LvlUp method when it occures
            { 
                _level = value;
                LvlUp();
            } 
        }
        // Base stats only changes when Hero lvls up
        public Stats BaseStats { get; set; }
        // Effective stats = base stats + equiped gear stats
        public Stats EffectiveStats { get; set; }
        public int XpToLevel { get; set; }
        public int Damage { get; set; }

        //Dict and List to hold weapon and armor.
        public Dictionary<ArmorSlots, Armor> Equipment = new Dictionary<ArmorSlots, Armor>();
        public List<Weapon> WeaponSlot = new List<Weapon>();


        //Constructor for all heroes
        protected Hero()
        {
            XpToLevel = 100;
            BaseStats = new Stats();

        }
  
        public virtual void LvlUp ()
        {
            //Overrided in child class
        }

        //Methdo to equip Armor Items. Checks if Hero is high enough lvl to equp and adds item to Equipment Dict.
        public void EquipItem(Armor armor)
        {
            if (armor.EquipmentLvl > Level)
                Console.WriteLine($"Your Hero needs to be lvl {armor.EquipmentLvl} to wear this item");
            else
            {
                if (Equipment.ContainsKey(armor.ArmorSlots))
                {
                    
                    Equipment[armor.ArmorSlots] = armor;
                }
                else
                {
                    Equipment.Add(armor.ArmorSlots, armor);
                }
                
                CalcEffectiveStats(BaseStats, Equipment);
            }

        }
        //Method applied in Equip Item method. Set Effective stats to baseStats + stats from equiped gear.
        public void CalcEffectiveStats(Stats baseStats, Dictionary<ArmorSlots, Armor> equipment)
        {
            EffectiveStats = new Stats();
            EffectiveStats.Health = baseStats.Health;
            EffectiveStats.Strength = baseStats.Strength;
            EffectiveStats.Dexterity = baseStats.Dexterity;
            EffectiveStats.Intelligence = baseStats.Intelligence;


            foreach (KeyValuePair<ArmorSlots, Armor> entry in equipment)
            {
                EffectiveStats.Health += entry.Value.ArmorBaseStats.Health;
                EffectiveStats.Strength += entry.Value.ArmorBaseStats.Strength;
                EffectiveStats.Dexterity += entry.Value.ArmorBaseStats.Dexterity;
                EffectiveStats.Intelligence += entry.Value.ArmorBaseStats.Intelligence;
            }
        }

        //Method to equip weapon. Implements "SetHeroDmg" from the weapons classes
        public void WieldWeapon(Weapon weapon)
        {
            if (weapon.WeaponLvl > Level)
            {
                Console.WriteLine($"Your Hero needs to be lvl {weapon.WeaponLvl} to wield this weapon");
            }
            else
            {
                if (WeaponSlot.Count == 0)
                {
                    WeaponSlot.Add(weapon);
                    Damage = weapon.IWeaponStrategy.SetHeroDmg(this.EffectiveStats, weapon.BaseDamage);
                }
                else
                {
                    WeaponSlot.Clear();
                    WeaponSlot.Add(weapon);
                    Damage = 0;
                    Damage = weapon.IWeaponStrategy.SetHeroDmg(this.EffectiveStats, weapon.BaseDamage);
                }
            }


        }




    }
}
