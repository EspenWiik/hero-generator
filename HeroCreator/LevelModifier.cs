﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Heroes.HeroCreator
{
    public class LevelModifier
    { // Class: Handles experience granting, calculations and increasing lvl
        
        // Method to grant XP to Hero and increase Hero lvl and 
        public static void GrantXp(Hero hero, int xp)
        {
            if (hero.XpToLevel - xp > 0)
            {
                hero.XpToLevel -= xp;
            }
            else
            {
                do
                {
                    hero.Level += 1;
                    xp = xp - hero.XpToLevel;
                    hero.XpToLevel = CalcNxtLvlXpReq(hero.Level);
                    
                } while (hero.XpToLevel < xp);
                hero.XpToLevel -= xp;
            }          
        }

        //Method that calculates the required experience based on the lvl of Hero
        private static int CalcNxtLvlXpReq(int lvl)
        {
            double reqXp = 100;
            for (int i = 1; i <= lvl; i++ )
            {
                reqXp = reqXp * 1.1;
            }
            int reqXP = (int)reqXp;

            return reqXP;
        }
    }
}
