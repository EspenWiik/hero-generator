﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Heroes.HeroCreator
{
    //Class which holds stats values, applied in both hero and armor classes.
    public class Stats
    {
        public int Health { get; set; }
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }
    }


}
