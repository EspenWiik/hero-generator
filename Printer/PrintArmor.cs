﻿using RPG_Heroes.Armory;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Heroes.Printer
{
    //Class uses to print Armor Details
    public class PrintArmor
    {
        public static void PrintArmorDetails(Armor armor)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"Name: {armor.Name}");
            sb.AppendLine($"Type: {armor.ArmorType}");
            sb.AppendLine($"Slot: {armor.ArmorSlots}");
            sb.AppendLine($"Level: {armor.EquipmentLvl}");
            if (armor.ArmorBaseStats.Strength > 0)
                sb.AppendLine($"Str: {armor.ArmorBaseStats.Strength}");

            if (armor.ArmorBaseStats.Dexterity > 0)
                sb.AppendLine($"Dex: {armor.ArmorBaseStats.Dexterity}");

            if (armor.ArmorBaseStats.Intelligence > 0)
                sb.AppendLine($"Int: {armor.ArmorBaseStats.Intelligence}");

            
            

            Console.WriteLine(sb);

        }
    }
}
