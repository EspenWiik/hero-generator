﻿using RPG_Heroes.HeroCreator;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Heroes.Printer
{
    //Class uses to print Hero Details
    public class PrintHero
    {
        public static void PrintHeroDetails (Hero hero)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"----Hero----");
            sb.AppendLine($"Class: {hero.GetType().Name}");
            sb.AppendLine($"Lvl: {hero.Level}");
            sb.AppendLine($"ExpReq: {hero.XpToLevel}");
            sb.AppendLine($"HP: {hero.EffectiveStats.Health}");
            sb.AppendLine($"Str: {hero.EffectiveStats.Strength}");
            sb.AppendLine($"Dex: {hero.EffectiveStats.Dexterity}");
            sb.AppendLine($"Int: {hero.EffectiveStats.Intelligence}");
            sb.AppendLine($"Damage: {hero.Damage}");

            Console.WriteLine(sb);


        }
    }
}
