﻿using RPG_Heroes.EquipmentStrat.Weapons;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPG_Heroes.Printer
{
    //Class uses to print Weapon Details
    class PrintWeapon
    {
        public static void PrintWeaponDetails(Weapon weapon)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("---Weapon---");
            sb.AppendLine($"Name: {weapon.Name}");
            sb.AppendLine($"Type: {weapon.GetType().Name}");
            sb.AppendLine($"WepLvl: {weapon.WeaponLvl}");
            sb.AppendLine($"BaseDmg: {weapon.BaseDamage}");

            Console.WriteLine(sb);


        }
    }
}
