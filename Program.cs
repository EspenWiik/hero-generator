﻿using RPG_Heroes.Armory;
using RPG_Heroes.EquipmentStrat.Armory;
using RPG_Heroes.EquipmentStrat.Armory.Armors;
using RPG_Heroes.EquipmentStrat.Weapons;
using RPG_Heroes.EquipmentStrat.Weponry.Wepons;
using RPG_Heroes.EquipmentStrat.Wepons;
using RPG_Heroes.HeroCreator;
using RPG_Heroes.HeroCreator.Classes;
using RPG_Heroes.Printer;
using System;

namespace RPG_Heroes
{
    class Program
    {
        static void Main(string[] args)
        {

            //CreateHeroAndGrantXp();
            //CreateWeapon();
            //CreateArmor();
            FullyEquipedWarrior();

        }
        public static void CreateHeroAndGrantXp()
        {
            Warrior warrior = new Warrior();
            LevelModifier.GrantXp(warrior, 1300);
            PrintHero.PrintHeroDetails(warrior);
        }

        public static void CreateWeapon()
        {
            Weapon sword = new Weapon("BadA** Sword", 5, WeaponType.Melee, new MeleeWeapon());
            PrintWeapon.PrintWeaponDetails(sword);
        }

        public static void CreateArmor()
        {
            Armor legs = new Armor("Legs of Glory", 4, ArmorType.LeatherArmor, new LeatherArmorStrategy(), ArmorSlots.Legs);
            PrintArmor.PrintArmorDetails(legs);
        }

        public static void FullyEquipedWarrior()
        {
            Warrior warrior = new Warrior();
            LevelModifier.GrantXp(warrior, 1300);
            Armor legs = new Armor("Legs of Glory", 4, ArmorType.PlateArmor, new PlateArmorStrategy(), ArmorSlots.Legs);
            Armor chest = new Armor("Breastplate of Glory", 4, ArmorType.PlateArmor, new PlateArmorStrategy(), ArmorSlots.Body);
            Armor head = new Armor("Helmet of Glory", 4, ArmorType.PlateArmor, new PlateArmorStrategy(), ArmorSlots.Head);
            warrior.EquipItem(legs);
            warrior.EquipItem(chest);
            warrior.EquipItem(head);
            Weapon sword = new Weapon("BadA** Sword", 5, WeaponType.Melee, new MeleeWeapon());
            warrior.WieldWeapon(sword);
            PrintHero.PrintHeroDetails(warrior);

            //add some noob gear
            Armor skirt = new Armor("Legs of Glory", 4, ArmorType.ClothArmor, new ClothArmorStrategy(), ArmorSlots.Legs);
            warrior.EquipItem(skirt);
            //Weapon staff = new Weapon("Bad Staff", 5, WeaponType.Magic, new MagicWeapon());
            //warrior.WieldWeapon(staff);
            PrintHero.PrintHeroDetails(warrior);
            Console.WriteLine(warrior.EffectiveStats.Strength);
        }





    }
}
