#README for Hero-Generator.

Hero Generator allows users to generate new heroes of the following classes:
-Warrior
-Ranger
-Mage

The classes have different attribute values (health, strength, dexterity, intelligence) which increase when they level up. 
In addition, the heroes can equip various items, ranging from armor items to weapons.
Also included are various print classes, that print the attributes of heroes, weapons and armor items.

Included in the Porgram.cs file are several methods that can be used as guidelines as to how to create heroes, create items and equip said items.

## System Usage:
Create Hero:
```
Warrior warrior = new Warrior();
```
Create Weapon:
```
Weapon sword = new Weapon("BadA** Sword", 5, WeaponType.Melee, new MeleeWeapon());
```
Create Armor:
```
Armor legs = new Armor("Legs of Glory", 4, ArmorType.LeatherArmor, new LeatherArmorStrategy(), ArmorSlots.Legs);
```
Grant Hero XP:
```
LevelModifier.GrantXp(warrior, 1300);
```
Equip Armo Item:
```
warrior.EquipItem(legs);
```

Equip Weapon:
```
warrior.WieldWeapon(sword);
```
Print methods:
```
PrintHero.PrintHeroDetails(warrior);
PrintWeapon.PrintWeaponDetails(sword);
PrintArmor.PrintArmorDetails(legs);
```
